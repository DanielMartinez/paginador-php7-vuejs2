new Vue({
    el: "#AppPaginador",
    data: {
        productos: [],
        pagination: {
            total: 0,
            current_page: 0,
            per_page: 0,
            last_page: 0,
            from: 0,
            to: 0,
        },
    },
    created() {
        this.obtenerProductos();
    },
    computed: {
        isActive() {
            return this.pagination.current_page;
        },
        pagesNumber() {
            let cantidad = 5;

            if (!this.pagination.to) {
                return [];
            }

            let from = this.pagination.current_page - cantidad;
            if (from < 1) from = 1;

            let to = from + cantidad * 2;
            if (to > this.pagination.last_page) to = this.pagination.last_page;

            const pageArray = [];
            while (from <= to) {
                pageArray.push(from);
                from++;
            }

            return pageArray;
        },
    },
    methods: {
        obtenerProductos(page) {
            return axios
                .get(`http://localhost/paginador-vuejs/app?page=${page}`)
                .then((response) => {
                    this.productos = response.data.productos;
                    this.pagination = response.data.pagination;
                });
        },
        changePage(page) {
            this.obtenerProductos(page);
        },
    },
});
