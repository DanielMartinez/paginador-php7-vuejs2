<?php

require '../core/app.php';
require '../core/DB.php';

$db = new Database();

if (
    !isset($_GET['page'])
    || empty($_GET['page'])
    || !is_numeric($_GET['page'])
) {
    $page = 1;
} else {
    $page = $_GET['page'];
}

$limit      = 5;
$total      = $db->consultar('SELECT COUNT(*) as total FROM products')->row();
$last_page  = ceil($total['total'] / $limit);

if ($page < 1) {
    $page = 1;
} else if ($page > $last_page) {
    $page = $last_page;
}

$offset     = ($page - 1) * $limit;
$productos  = $db->consultar("SELECT id, name, short, body FROM products ORDER BY id LIMIT $offset, $limit")->all();

header('Content-Type: application/json');
echo json_encode([
    'productos' => $productos,
    'pagination' => [
        'total'         => intval($total['total']),
        'current_page'  => intval($page),
        'last_page'     => ceil($total['total'] / $limit),
        'per_page'      => intval($limit),
        'from'          => intval($offset + 1),
        'to'            => intval($offset + $limit)
    ]
]);
exit();
