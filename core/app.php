<?php

declare(strict_types=1);

$config = parse_ini_file('config.ini', false);

foreach ($config as $key => $value) {
    define($key, $value);
}
